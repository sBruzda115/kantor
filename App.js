import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { fetchExchangeRate } from "./util/http";
import Input from "./components/Input";
import Button from "./components/Button";
import Output from "./components/OutPut";
import Loading from "./components/Loading";

export default function App() {
  const [amountPLN, setAmountPLN] = useState("");
  const [currency, setCurrency] = useState("");
  const [feching, setFeching] = useState(false);
  const [exchangeRate, setExchangeRate] = useState(null);

  function handleFetchExchangeRate() {
    fetchExchangeRate(amountPLN, currency, setExchangeRate, setFeching);
  }

  const convertCurrency = () => {
    if (!isNaN(amountPLN) && exchangeRate) {
      const result = parseFloat(amountPLN) / exchangeRate;
      return result.toFixed(2);
    }
    return "";
  };
  let rate = 1;
  if (exchangeRate !== null) {
    rate = exchangeRate.toFixed(2);
  } else {
    rate = 1;
  }

  if (feching) {
    return <Loading />;
  }

  return (
    <LinearGradient
      colors={["#9a5c35", "#58311e", "#140d0a"]}
      style={styles.container}
    >
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Kantor</Text>
      </View>
      <View style={styles.inputContainer}>
        <Input placeholder={"EUR"} set={setCurrency} value={currency} />
        <Input placeholder={"00.0zł"} set={setAmountPLN} value={amountPLN} />
      </View>
      <View style={styles.rateConteiner}>
        <Text
          style={styles.textRate}
        >{`Aktualny kurs ${currency}: ${rate}`}</Text>
      </View>
      <View style={styles.outputContainer}>
        <Output
          currency={currency}
          amount={convertCurrency()}
          plnAmount={amountPLN}
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          onPress={handleFetchExchangeRate} /*onPress={fetchExchangeRate}*/
        />
      </View>
      <StatusBar style="light" />
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#683014",
  },
  title: {
    fontSize: 50,
    fontWeight: "bold",
    color: "#ffca7b",
  },
  titleContainer: {
    marginTop: 150,
    alignItems: "center",
    borderBottomWidth: 5,
    borderColor: "#ffca7b",
    marginHorizontal: 60,
  },
  inputContainer: {
    flexDirection: "row",
    marginTop: 40,
    alignItems: "center",
    marginHorizontal: 5,
  },
  outputContainer: {
    alignItems: "center",
    marginTop: 10,
  },
  buttonContainer: {
    alignItems: "center",
    marginTop: 40,
  },
  textRate: {
    fontSize: 25,
    color: "#ffca7b",
    paddingBottom: 0,
    paddingTop: 10,
    fontWeight: "bold",
  },
  rateConteiner: {
    alignItems: "center",
    marginTop: 40,
  },
});
