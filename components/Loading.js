import { StyleSheet, View, ActivityIndicator } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

function Loading() {
  return (
    <LinearGradient
      colors={["#9a5c35", "#58311e", "#140d0a"]}
      style={style.loading}
    >
      <ActivityIndicator size={"large"} color={"black"} />
    </LinearGradient>
  );
}

export default Loading;

const style = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 24,
  },
});
