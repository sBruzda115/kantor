import React from "react";
import { View, Text, StyleSheet } from "react-native";

function Output({ currency, amount, plnAmount }) {
  return (
    <View style={styles.textContainer}>
      <Text
        style={styles.infotext}
      >{`${plnAmount} PLN przliczone na ${currency}:`}</Text>
      <Text style={styles.text}>{`${amount}`}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  textContainer: {
    alignItems: "center",
    borderColor: "#000",
    borderBottomWidth: 6,
    borderTopWidth: 6,
  },
  infotext: {
    fontSize: 25,
    color: "#ffca7b",
    paddingBottom: 5,
    paddingTop: 30,
    fontWeight: "bold",
  },
  text: {
    fontSize: 40,
    fontWeight: "bold",
    color: "#ffca7b",
    padding: 30,
  },
});

export default Output;
