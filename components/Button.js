import React from "react";
import { View, Pressable, Text, StyleSheet } from "react-native";

function Button({ onPress }) {
  return (
    <View style={styles.rootContainer}>
      <Pressable
        style={({ pressed }) =>
          pressed ? [styles.button, styles.pressedButton] : styles.button
        }
        onPress={onPress}
      >
        <View style={styles.textContainer}>
          <Text style={styles.text}>Convert</Text>
        </View>
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    borderWidth: 3,
    borderColor: "#febf68",
    padding: 20,
  },
  button: {
    backgroundColor: "",
  },
  pressedButton: {
    opacity: 0.5,
  },
  text: {
    fontSize: 40,
    color: "#febf68",
  },
  textContainer: {
    alignItems: "center",
  },
});

export default Button;
