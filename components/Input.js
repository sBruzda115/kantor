import React from "react";
import { TextInput, StyleSheet } from "react-native";

function Input({ placeholder, set, value }) {
  return (
    <TextInput
      style={styles.textInput}
      placeholder={placeholder}
      placeholderTextColor={"#5e5c5a"}
      color={"#ffca7b"}
      maxLength={5}
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
      value={value}
      onChangeText={set}
      textAlign="center"
    />
  );
}

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    borderColor: "#ffca7b",
    borderWidth: 3,
    borderRadius: 8,
    fontSize: 40,
    padding: 10,
    marginHorizontal: 30,
  },
});

export default Input;
