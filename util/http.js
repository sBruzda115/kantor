import axios from "axios";
import { Alert } from "react-native";

const threeLetterPattern = /^[A-Z]{3}$/;

export function fetchExchangeRate(
  amountPLN,
  currency,
  setExchangeRate,
  setFeching
) {
  if (amountPLN === "" && currency === "") {
    Alert.alert("Error", "Enter the data!", [{ text: "OK", style: "cancel" }]);
  } else if (isNaN(amountPLN)) {
    Alert.alert("Error", "Invalid PLN amount!", [
      { text: "OK", style: "cancel" },
    ]);
  } else if (
    typeof currency === "string" &&
    threeLetterPattern.test(currency)
  ) {
    setFeching(true);
    axios
      .get(`http://api.nbp.pl/api/exchangerates/rates/a/${currency}/`)
      .then((response) => {
        setExchangeRate(response.data.rates[0].mid);
      })
      .catch((error) => {
        Alert.alert("Error", "Invalid request!", [
          { text: "OK", style: "cancel" },
        ]);
      })
      .finally(() => {
        setFeching(false);
      });
  } else {
    Alert.alert("Error", "Invalid currency!", [
      { text: "OK", style: "cancel" },
    ]);
  }
}
